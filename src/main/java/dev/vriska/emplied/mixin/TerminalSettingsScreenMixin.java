package dev.vriska.emplied.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import appeng.client.gui.me.common.TerminalSettingsScreen;
import net.minecraft.text.Text;

@Mixin(TerminalSettingsScreen.class)
public class TerminalSettingsScreenMixin {
	@ModifyVariable(method = "<init>(Lappeng/client/gui/me/common/MEStorageScreen;)V", at = @At("STORE"), name = "hasExternalSearch", remap = false)
	private boolean hasExternalSearch(boolean hasExternalSearch) {
		return true;
	}

	@ModifyVariable(method = "<init>(Lappeng/client/gui/me/common/MEStorageScreen;)V", at = @At("STORE"), name = "externalSearchMod", remap = false)
	private Text externalSearchMod(Text externalSearchMod) {
		return Text.literal("EMI");
	}
}
