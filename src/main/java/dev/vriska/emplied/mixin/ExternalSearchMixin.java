package dev.vriska.emplied.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import appeng.util.ExternalSearch;
import dev.emi.emi.screen.EmiScreenManager;

@Mixin(ExternalSearch.class)
public class ExternalSearchMixin {
	@Inject(method = "isExternalSearchAvailable", at = @At("HEAD"), cancellable = true, remap = false)
	private static void isExternalSearchAvailable(CallbackInfoReturnable<Boolean> ci) {
		ci.setReturnValue(true);
	}

	@Inject(method = "getExternalSearchText", at = @At("HEAD"), cancellable = true, remap = false)
	private static void getExternalSearchText(CallbackInfoReturnable<String> ci) {
		// im pretty sure this isnt a public api but whatever
		ci.setReturnValue(EmiScreenManager.search.getText());
	}

	@Inject(method = "setExternalSearchText", at = @At("HEAD"), cancellable = true, remap = false)
	private static void setExternalSearchText(String text, CallbackInfo ci) {
		EmiScreenManager.search.setText(text);
		ci.cancel();
	}

	@Inject(method = "clearExternalSearchText", at = @At("HEAD"), cancellable = true, remap = false)
	private static void clearExternalSearchText(CallbackInfo ci) {
		EmiScreenManager.search.setText("");
		ci.cancel();
	}

	@Inject(method = "isExternalSearchFocused", at = @At("HEAD"), cancellable = true, remap = false)
	private static void isExternalSearchFocused(CallbackInfoReturnable<Boolean> ci) {
		ci.setReturnValue(EmiScreenManager.search.isFocused());
	}
}
